package main

import (
	"flag"
	"fmt"
	"log/slog"

	"context"
	"log"

	"gitlab.com/spock/dasango"
	"gitlab.com/spock/dasango/ncc"

	"github.com/ziutek/mymysql/mysql"
	_ "github.com/ziutek/mymysql/native" // Native engine

	//"log/syslog"
	"net"
	"os"

	"github.com/spf13/viper"

	//	"path"

	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
)

// Port     int
// Username string
// Password string
// Dbname   string
// }
const VERSION string = "0.7.0-09137b4"

func checkError(err error) {
	if err != nil {
		log.Println(err)
	}
}

func checkeSQLResult(rows []mysql.Row, res mysql.Result, err error) ([]mysql.Row,
	mysql.Result) {
	checkError(err)
	return rows, res
}

func main() {
	// cli options
	community := "public"
	//rrdOutputDir := flag.String("rrdout", "/tmp/onu-rrds", "Output directory for auto created RRD files")
	//oltList := flag.String("olt", "10.9.0.9:public", "Comma separated list of OLT's to query and community i.e: 10.0.0.1:public")
	timeout := flag.Int("timeout", 10, "Timeout for handling connections to OLT's")
	logfile := flag.String("log", "", "Optional log output (default stderr)")
	sqlenable := flag.Bool("sqlenable", false, "Enable OLD_ID ONU_ID updates for NCC database")
	influxon := flag.Bool("influxon", false, "Enable stats export to InfluxDB")
	systemd := flag.Bool("systemd", false, "Disable cron/batch mode and work as a daemon")
	period := flag.String("period", "1m", "For systemd mode only. How often make OLT's queries i.e 1m,60s,etc")
	//is_debug := flag.Bool("debug", false, "Enable debug logging")
	flag.Parse()
	log.SetOutput(os.Stderr)
	viper.SetConfigName("onustats")       // name of config file (without extension)
	viper.AddConfigPath("/etc/onustats/") // path to look for the config file in
	viper.AddConfigPath("$HOME/.appname") // call multiple times to add many search paths
	viper.AddConfigPath(".")              // optionally look for config in the working directory
	viper.SetConfigType("json")
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		log.Fatalf("Fatal error config file: %s \n", err)
		os.Exit(1)
	}
	if len(*logfile) > 0 {
		f, err := os.OpenFile(*logfile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0640)
		if err != nil {
			slog.Warn("Error opening file", "error", err)
			os.Exit(-1)
		}
		defer f.Close()
		if err != nil {
			log.Println("Unable to open logfile:", err)
		} else {
			log.SetOutput(f)
		}
	}
	log.Println("onustats version", VERSION, "Marcin Jurczuk <marcin@jurczuk.eu>")
	//olts := strings.Split(*oltList, ",")
	ncc := ncc.NewNCC()
	dburi := fmt.Sprintf("%s:%s@tcp(%s:%.0f)/%s?parseTime=True",
		viper.Get("sql.connection.username").(string),
		viper.Get("sql.connection.password").(string),
		viper.Get("sql.connection.hostname").(string),
		viper.Get("sql.connection.port"),
		viper.Get("sql.connection.dbname").(string))
	err = ncc.Connect(dburi)
	if err != nil {
		slog.Warn("Erorr establishing MySQL connection", "error", err)
		os.Exit(-1)
	}
	olts, err := ncc.GetAllOLTs()
	if err != nil {
		slog.Warn("Error retrieving OLT list! Aboring!", "error", err)
	}
	fmt.Println("Following OLT's found in database:")
	for _, olt := range olts {
		fmt.Println("Name:", olt.Name, "IP:", olt.IPAddr, "Vendor", olt.Vendor)
	}
	finishCnt := len(olts)
	sqlchan := make(chan string, 10)
	if *sqlenable {
		log.Println("SQl updates enabled !")
		go processSQLCmd(sqlchan)
	}
	influxchan := make(chan dasango.ONU, 10)
	if *influxon {
		log.Println("Exporting ONU stats to InfluxDB")
		go processInflux(influxchan)
	}
	if *systemd {
		td, err := time.ParseDuration(*period)
		if err != nil {
			log.Fatalln("Unable to parse period argument:", err)
			os.Exit(1)
		}
		log.Println("Querying OLT's every", td)
		ticker := time.NewTicker(td)
		for t := range ticker.C {
			log.Println("Starting main task at ", t)
			done := make(chan string, len(olts))
			for _, olt := range olts {

				log.Println("Fetching data from", olt)
				go processOlt(olt.IPAddr, olt.RoCommunity, olt.Name, *sqlenable, sqlchan, *influxon, influxchan, done)
			}

		}
	} else {

		done := make(chan string, len(olts))
		for _, olt := range olts {
			log.Println("Fetching data from", olt)
			go processOlt(olt.IPAddr, community, olt.Name, *sqlenable, sqlchan, *influxon, influxchan, done)
		}

		for {
			select {
			case msg := <-done:
				log.Println("Querying", msg, "finished")
				finishCnt--
				if finishCnt == 0 {
					log.Println("Done for all OLT's")
					os.Exit(0)
				}

			case <-time.After((time.Duration(*timeout) * time.Second)):
				log.Println("Timeout")
				os.Exit(1)
			}
		}

	}
}
func processInflux(data chan dasango.ONU) {
	// make InfluxConnection
	client := influxdb2.NewClient(viper.Get("influxdb.url").(string), viper.Get("influxdb.token").(string))
	writeAPI := client.WriteAPIBlocking(viper.Get("influxdb.org").(string), viper.Get("influxdb.bucket").(string))
	for {
		select {
		case onu := <-data:
			p := fmt.Sprintf("onu_stats,serial=%s,port_id=%d,ont_id=%d rxlevel=%.2f", onu.Serial, onu.OltId, onu.Id, onu.RxLevel)
			//log.Println("INFLUX:", p)
			err := writeAPI.WriteRecord(context.Background(), p)
			if err != nil {
				log.Println("Error writing to InfluxDB:", err)
			}

		}
	}
}
func processSQLCmd(data chan string) {
	var err error
	dbready := true
	uri := fmt.Sprintf("%s:%.0f", viper.Get("sql.connection.hostname"), viper.Get("sql.connection.port"))
	//fmt.Println("DB URI:", uri)
	db := mysql.New("tcp", "", uri, viper.Get("sql.connection.username").(string), viper.Get("sql.connection.password").(string), viper.Get("sql.connection.dbname").(string))
	err = db.Connect()
	//defer db.Close()
	if err != nil {
		log.Fatalln("Database connection failed:", err)
		dbready = false
	}
	for {
		select {
		case msg := <-data:
			if dbready {
				checkeSQLResult(db.Query(msg))

			} else {
				log.Fatalln("Failed to execute SQL statement:", msg)
			}
		}
	}
}

func processOlt(oltip string, snmpcommunity string, oltname string, updatedb bool, dbchan chan string, influxexport bool, infchan chan dasango.ONU, done chan string) {
	log.Println("Goroutine launched for OLT", oltip)
	var err error

	//olt := dasango.MakeOLT("malkolt01")
	olt := dasango.MakeOLT(oltname)
	olt.SNMPSession.Community = snmpcommunity

	olt.IPAddress, err = net.ResolveIPAddr("ip", oltip)
	if err != nil {
		log.Fatalln("Unable to resolve IP for", oltip)
	}
	err = olt.Connect()
	if err != nil {
		log.Fatalln("Connection problem with", oltip)
	}
	err = olt.GetONUList()

	//	olt.AddONU(dasango.ONU{Id: 1, OltId: 2, Serial: "DSN0000", RxLevel: 0})
	if err != nil {
		log.Println(err)
	}
	log.Println(len(olt.ONUs), "ONT's parsed at", oltip)
	onus, err := olt.GetONURxLevels()
	if err != nil {
		log.Println(fmt.Sprintf("GetONURxLevels(%s) error: %s", oltip, err))
	}
	if updatedb {
		//log.Println("Cleaning table old data before update for OLT", oltip)
		dbchan <- fmt.Sprintf("UPDATE %s set `%s`=null, onu_id=null where olt_ip = INET_ATON('%s')", viper.Get("sql.dbconfig.onutable"), viper.Get("sql.dbconfig.olt_id_col"), oltip)
	}
	for _, onu := range onus {
		//filepath := path.Join(rrdout, fmt.Sprintf("%s-rxlevel.rrd", onu.Serial))
		//if _, err := os.Stat(filepath); os.IsNotExist(err) {
		//	err = graph.CreateRXLevelsRRD(filepath, 60)
		//	if err != nil {
		//		log.Println("Error creating graph:", err)
		//	}
		//}
		//graph.UpdateRXLevelsRRD(filepath, int(onu.RxLevel*10))
		if updatedb {
			dbchan <- fmt.Sprintf("UPDATE %s set `%s` = '%d', onu_id = '%d', olt_ip = INET_ATON('%s') where serial ='%s'", viper.Get("sql.dbconfig.onutable"), viper.Get("sql.dbconfig.olt_id_col"), onu.OltId, onu.Id, oltip, onu.Serial)
		}
		if influxexport {
			infchan <- onu
		}
		//log.Println(onu.Serial, "->", onu.OltId, ":", onu.Id)
		//fmt.Printf("ONU %s RxLevel: %f\n", onu.Serial, onu.RxLevel)
	}
	done <- oltip
}
