package dasango

import (
	"fmt"
	"net"
	"strconv"
	"strings"
	"time"

	"log"

	"github.com/gosnmp/gosnmp"
)

const (
	sysName                       = "1.3.6.1.2.1.1.5.0"
	ONU_SERIAL                    = "1.3.6.1.4.1.6296.101.23.3.1.1.4"
	ONU_RX_LEVEL                  = "1.3.6.1.4.1.6296.101.23.3.1.1.16"
	ONU_PROFILE                   = ".1.3.6.1.4.1.6296.101.23.3.1.1.8"
	GPON_sleGponOnuControlRequest = ".1.3.6.1.4.1.6296.101.23.3.2.1"
	GPON_sleGponOnuControlOltId   = ".1.3.6.1.4.1.6296.101.23.3.2.6"
	GPON_sleGponOnuControlId      = ".1.3.6.1.4.1.6296.101.23.3.2.7"
	GPON_sleGponOnuControlTimer   = ".1.3.6.1.4.1.6296.101.23.3.2.3"
)

type ONU struct {
	Id          int
	OltId       int
	Serial      string
	RxLevel     float32
	Description string
}

type OLT struct {
	Name        string
	IPAddress   *net.IPAddr
	Shelf_type  string
	ONUs        []ONU
	SNMPSession *gosnmp.GoSNMP
}

func MakeOLT(name string) *OLT {
	var olt = OLT{}
	olt.Name = name
	olt.SNMPSession = &gosnmp.GoSNMP{}
	//olt.SNMPSession = gosnmp.Default
	//      olt.SNMPSession.Logger = log.New(os.Stderr, "", 0)
	olt.SNMPSession.Port = 161
	olt.SNMPSession.Version = gosnmp.Version2c
	olt.SNMPSession.Community = "public"
	olt.SNMPSession.Retries = 3
	olt.SNMPSession.Timeout = time.Duration(3) * time.Second
	//	olt.ONUs = make([]ONU, 128)
	return &olt
}
func (o *OLT) ResolveIP() (err error) {
	o.IPAddress, err = net.ResolveIPAddr("ip", o.Name)
	return err
}
func (o *OLT) SetCommunity(community string) {
	o.SNMPSession.Community = community
}

func (o *OLT) Connect() (err error) {
	if o.IPAddress == nil {
		err = o.ResolveIP()
		if err != nil {
			return err
		}
	}
	o.SNMPSession.Target = o.IPAddress.IP.String()
	err = o.SNMPSession.Connect()
	//	defer o.SNMPSession.Conn.Close()
	return err
}
func (o *OLT) FindONUById(olt_id int, onu_id int) (ont *ONU) {
	for _, v := range o.ONUs {
		if v.OltId == olt_id && v.Id == onu_id {
			ont = &v
			return ont
		}
	}
	return
}
func (o *OLT) GetDeviceName() (string, error) {
	var pdus []string
	pdus = append(pdus, sysName)
	rs, err := o.SNMPSession.Get(pdus)
	if err != nil {
		return "", err
	}
	deviceName := string(rs.Variables[0].Value.([]byte))
	return deviceName, nil
}
func (o *OLT) ListUnregisteredONU() ([]string, error) {
	result := make([]string, 0)

	err := o.SNMPSession.BulkWalk(ONU_PROFILE, func(pdu gosnmp.SnmpPDU) error {
		portID, err := GetOnuOltId(pdu.Name)
		if err != nil {
			log.Println("Error getting portID:", err.Error())
			return err
		}
		ontID, err := GetOnuId(pdu.Name)
		if err != nil {
			log.Println("Error getting ontID", err.Error())
			return err
		}
		ontProfile := string(pdu.Value.([]byte))
		if len(ontProfile) == 0 {
			//fmt.Println("Got unreg ONT:", pdu.Name)
			result = append(result, fmt.Sprintf("%d.%d", portID, ontID))
		}
		return err
	})
	return result, err
}

func (o *OLT) AddONU(newonu ONU) []ONU {
	o.ONUs = append(o.ONUs, newonu)
	return o.ONUs
}
func (o *OLT) GetONTSerial(port_id int, ont_id int) (string, error) {
	var serial string
	var err error
	oid := make([]string, 1)
	oid[0] = fmt.Sprintf("%s.%d.%d", ONU_SERIAL, port_id, ont_id)
	rs, err := o.SNMPSession.Get(oid)
	if len(rs.Variables) == 1 {
		switch rs.Variables[0].Type {
		case gosnmp.OctetString:
			serial = string(rs.Variables[0].Value.([]byte))
		}
		return serial, err
	}
	return serial, err
}

func (o *OLT) GetONUList() (err error) {
	var onus []ONU
	if o.SNMPSession.Conn == nil {
		fmt.Println("Establishing missing connection")
		err = o.Connect()
		if err != nil {
			return err
		}
	}
	err = o.SNMPSession.BulkWalk(ONU_SERIAL, func(pdu gosnmp.SnmpPDU) (err error) {
		olt_id, err := GetOnuOltId(pdu.Name)
		onu_id, err := GetOnuId(pdu.Name)
		if err != nil {
			fmt.Println(err)
			//fmt.Printf("%s = ", pdu.Name)
		} else {
			onu := o.FindONUById(olt_id, onu_id)
			if onu == nil {
				//				fmt.Fprintf(os.Stdout, "New onu discovered - adding to ONU list (%d:%d %s)\n", olt_id, onu_id, string(pdu.Value.([]byte)))
				newonu := ONU{onu_id, olt_id, string(pdu.Value.([]byte)), -40, ""}
				onus = append(onus, newonu)
			} else {
				fmt.Println("ONU exists", onu)
			}
		}
		//		switch pdu.Type {
		//		case gosnmp.OctetString:
		//			b := pdu.Value.([]byte)
		//			fmt.Printf("%s\n", string(b))
		//		default:
		//			fmt.Printf("TYPE %d: %d\n", pdu.Type, gosnmp.ToBigInt(pdu.Value))
		//		}
		return err
	})
	o.ONUs = onus
	return err

}
func (o *OLT) GetONURxLevels() (onus []ONU, err error) {
	if o.SNMPSession.Conn == nil {
		//fmt.Println("Establishing missing connection")
		err = o.Connect()
		if err != nil {
			return
		}
	}
	if len(o.ONUs) == 0 {
		//fmt.Fprintf(os.Stderr, "No ONU found - updating list\n")
		err = o.GetONUList()
		if err != nil {
			return
		}
	}
	//fmt.Println("ONU count after update:", len(o.ONUs))
	err = o.SNMPSession.BulkWalk(ONU_RX_LEVEL, func(pdu gosnmp.SnmpPDU) (err error) {
		olt_id, err := GetOnuOltId(pdu.Name)
		onu_id, err := GetOnuId(pdu.Name)
		if err != nil {
			fmt.Println(err)
			fmt.Printf("%s = ", pdu.Name)
		} else {
			onu := o.FindONUById(olt_id, onu_id)
			if onu != nil {
				RxLevel := float32((int(pdu.Value.(int)))) / 10
				onu.RxLevel = RxLevel
				onus = append(onus, *onu)
			}
		}
		return
	})
	return

}
func (olt *OLT) ReadONURxLevel(onu *ONU) (rxlevel float32, err error) {
	var onu_rx_oid []string
	oid := fmt.Sprintf("%s.%d.%d", ONU_RX_LEVEL, onu.OltId, onu.Id)
	onu_rx_oid = append(onu_rx_oid, oid)
	r, err := olt.SNMPSession.Get(onu_rx_oid)
	if err != nil {
		return
	}
	// ogarnac bledy po typie
	for _, variable := range r.Variables {
		//tmplevel := gosnmp.ToBigInt(variable.Value)
		rxlevel = float32((int(variable.Value.(int)))) / 10
		onu.RxLevel = rxlevel
		//rxlevel = float32(tmplevel)
	}
	return

}
func (olt *OLT) ResetONU(PortID uint16, ONUID uint16) error {
	// request type - ONU reset
	pdu := []gosnmp.SnmpPDU{gosnmp.SnmpPDU{Name: GPON_sleGponOnuControlRequest, Type: gosnmp.Integer, Value: 8}}
	_, err := olt.SNMPSession.Set(pdu)
	if err != nil {
		return err
	}
	// select port for ONT
	pdu = []gosnmp.SnmpPDU{gosnmp.SnmpPDU{Name: GPON_sleGponOnuControlOltId, Type: gosnmp.Integer, Value: int(PortID)}}
	_, err = olt.SNMPSession.Set(pdu)
	if err != nil {
		return err
	}
	// select onu id
	pdu = []gosnmp.SnmpPDU{gosnmp.SnmpPDU{Name: GPON_sleGponOnuControlId, Type: gosnmp.Integer, Value: int(ONUID)}}
	_, err = olt.SNMPSession.Set(pdu)
	if err != nil {
		return err
	}
	// execute now
	pdu = []gosnmp.SnmpPDU{gosnmp.SnmpPDU{Name: GPON_sleGponOnuControlTimer, Type: gosnmp.Integer, Value: 0}}
	_, err = olt.SNMPSession.Set(pdu)
	if err != nil {
		return err
	}
	return nil

}
func GetOnuOltId(oid string) (id int, err error) {
	elements := strings.Split(oid, ".")
	id, err = strconv.Atoi(elements[len(elements)-2])
	return
}
func GetOnuId(oid string) (id int, err error) {
	elements := strings.Split(oid, ".")
	id, err = strconv.Atoi(elements[len(elements)-1])
	return
}
