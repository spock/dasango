package main

import (
	"flag"
	"fmt"
	"os"
	"strconv"

	"github.com/gosnmp/gosnmp"
	"gitlab.com/spock/dasango"

	//"log"
	"net"
	//"os"
	"strings"
	"time"
)

var (
	ip = flag.String("ip", "10.255.255.242", "OLT IP to query")
)

func main() {
	var err error
	//olt := dasango.MakeOLT("malkolt01")
	olt := dasango.OLT{Name: "malkolt01"}
	oltIP := *ip
	olt.IPAddress, err = net.ResolveIPAddr("ip", oltIP)
	fmt.Println("Connecting to ", oltIP)
	if err != nil {
		fmt.Print("Unable to resolve IP")
	}
	olt.SNMPSession = &gosnmp.GoSNMP{}
	//olt.SNMPSession = gosnmp.Default
	//	olt.SNMPSession.Logger = log.New(os.Stderr, "", 0)
	olt.SNMPSession.Port = 161
	olt.SNMPSession.Version = gosnmp.Version2c
	olt.SNMPSession.Community = "public"
	olt.SNMPSession.Retries = 3
	olt.SNMPSession.Timeout = time.Duration(3) * time.Second
	err = olt.Connect()
	if err != nil {
		fmt.Println("Connetion problem")
	}

	data, err := olt.ListUnregisteredONU()

	//	olt.AddONU(dasango.ONU{Id: 1, OltId: 2, Serial: "DSN0000", RxLevel: 0})
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	for _, ont := range data {
		portId, _ := strconv.Atoi(strings.Split(ont, ".")[0])
		ontId, _ := strconv.Atoi(strings.Split(ont, ".")[1])
		serial, _ := olt.GetONTSerial(portId, ontId)
		fmt.Println("Unregistered ONT:", ont, "(", serial, ")")
	}
	//	fmt.Println("ONU list:")
	//	fmt.Println(olt.ONUs)
	// for _, onu := range olt.ONUs {
	// 	fmt.Printf("ONU %v RxLevel:", onu)

	// 	_, err := olt.ReadONURxLevel(&onu)
	// 	if err != nil {
	// 		fmt.Println(err)
	// 	}
	// 	fmt.Println(onu.RxLevel, "dB")

	// }
}
