package ncc

import (
	"database/sql"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type NCC struct {
	Db *gorm.DB
}

func NewNCC() NCC {
	return NCC{
		Db: nil,
	}

}

func (n *NCC) Connect(dburi string) error {

	db, err := gorm.Open(mysql.Open(dburi), &gorm.Config{})
	if err != nil {
		return err
	}
	n.Db = db
	return nil
}

func (n *NCC) GetAllOLTs() ([]DbOLT, error) {
	var olts []DbOLT
	err := n.Db.Find(&olts).Error
	return olts, err
}

type DbOLT struct {
	Id          int            `gorm:"primaryKey"`
	Name        string         `gorm:"column:name"`
	IPAddr      string         `gorm:"column:ipaddress"`
	Description sql.NullString `gorm:"column:description"`
	Vendor      string         `gorm:"column:vendor"`
	RoCommunity string         `gorm:"column:rocommunity"`
	RwCommunity string         `gorm:"column:rwcommunity"`
}

func (d DbOLT) TableName() string {
	return "olts"
}

// type DbONT struct {
// 	Id           int `gorm:"primaryKey"`
// 	Serial       string
// 	Owner        string
// 	OnuprofileId int
// 	OnutypeId    int
// 	Active       int
// 	CreatedAt    time.Time

// }
